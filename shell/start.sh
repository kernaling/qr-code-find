#/bin/bash
echo $0
NOW_DIR=`dirname $0`
BASE_DIR=`cd $NOW_DIR && cd .. && pwd`
echo " 当前目录: $BASE_DIR "
cd $BASE_DIR && nohup python3 deploy/python/infer_qrcode.py > /dev/null &
